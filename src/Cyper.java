import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.crypto.Cipher;
import java.security.*;


public class Cyper {
	
	
	public Cyper(){
		
	}
	
	public static void main(String args[]){
		KeyPair keypair = null;
		PublicKey puk = null;
		PrivateKey pvk = null;
		String signature_algorithm_text = "SHA1withRSA";
		
		/**
		 * Entero que tipifica el tipo de algoritmo de firma
		 */
		int signature_algorithm;	
		/**
		 * String para almacenar el nombre del archivo
		 */
		String file;
		/**
		 * Entero que tipifica el tipo de algoritmo de cifrado
		 */
		
		if(args.length!=4){
			System.out.println("Error in parameters");
			System.out.println("Correct usage: java -jar cypher.jar [file] [key] [mode] [action]");
			System.out.println("If you have no key, a key with the given name will be created");
			System.out.println("Keys should have .key extension");
			System.out.println("Avalible signature modes:");
			System.out.println("0: SHA1withRSA");
			System.out.println("1: MD2withRSA");
			System.out.println("2: MD5withRSA");
			System.out.println("Avalibre actions");
			System.out.println("1: Cypher");
			System.out.println("2: Decipher");
			System.out.println("3: Signature");
			System.out.println("4: Check signature");
			
			
		}
		else{
			file = args[0];
			
			signature_algorithm = Integer.parseInt(args[2]);
			
			switch(signature_algorithm){
			case 0:
				signature_algorithm_text = "SHA1withRSA"; 
				break;
			case 1:
				signature_algorithm_text = "MD2withRSA"; 
				break;
			case 2:
				signature_algorithm_text = " MD5withRSA"; 
				break;
			}

			String llaves = args[1];
			File f = new File(llaves);
			if(f.exists() && !f.isDirectory()){
				try{
				FileInputStream fis = new FileInputStream(llaves);
				ObjectInputStream ois = new ObjectInputStream(fis);
				keypair = (KeyPair) ois.readObject();
				 puk = keypair.getPublic();
		         pvk = keypair.getPrivate();
				ois.close();
				fis.close();
				}catch(Exception e){
					System.out.println("Error reading keys");
				}
			}
			else{
				//Si no, se crea una nueva
				try{
				 	KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		            kpg.initialize(512);

		            keypair = kpg.generateKeyPair();
		            puk = keypair.getPublic();
		            pvk = keypair.getPrivate();

		            FileOutputStream fs = new FileOutputStream(llaves);
		            ObjectOutputStream oos = new ObjectOutputStream(fs);
		            oos.writeObject(keypair);
		            fs.flush();
		            oos.close();
		            fs.close();
				}catch (Exception e){
					System.out.println("Error creating key");
				}
			}
			
			System.out.println("Your keys:");
			System.out.println("Private key: " + keypair.getPrivate().toString());
			System.out.println("Public key: " + keypair.getPublic().toString());
			System.out.println();
			
			if(Integer.parseInt(args[3]) == 1)
				cypher(file, keypair);
			if(Integer.parseInt(args[3]) == 2)
				decipher(file, pvk);
			if(Integer.parseInt(args[3]) == 3)
				signature(file, keypair,signature_algorithm_text);
			if(Integer.parseInt(args[3]) == 4)
				check_signature(file, keypair);
			
		}

	}
	
	public static void decipher(String file, PrivateKey pvk) {
		boolean success = true;
		String salida = "ERROR";
		try{
			 	Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		        c.init(Cipher.DECRYPT_MODE, pvk);

		        // Abrir flujos de lectura y escritura
		        FileInputStream fis = new FileInputStream(file);
		        salida = file
						.replaceAll("\\.\\w+", "") + ".cla";
		        FileOutputStream fos = new FileOutputStream(salida);

		        int blockSize = 64;
		        byte[] b = new byte[blockSize];
		        int read = 0;
		        while ((read = fis.read(b)) != -1) {
		        	fos.write(c.doFinal(b,0,read));
		           
		        }
		        fos.flush();
		        fis.close();
		        fos.close();
		}catch(Exception e){
			System.out.println("Error in decypher!");
			success = false;
		}
		if(success)
			System.out.println("Decipher success! Check new file: " + salida );
		}

	public static void cypher(String file, KeyPair keyPair) {
		boolean success = true;
		String salida = "ERROR";
		try{
			if (keyPair != null) {
				salida = file
						.replaceAll("\\.\\w+", "") + ".cif";
				FileOutputStream output = new FileOutputStream(salida);
				FileInputStream input = new FileInputStream(file);
				Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
				c.init(c.ENCRYPT_MODE, keyPair.getPublic());
				byte[] block = new byte[53];
				int i = 0;
				while ((i = input.read(block)) != -1) {
					output.write(c.doFinal(block,0,i));
				}

				output.flush();
				input.close();
				output.close();

			}
		}catch(Exception e){
			System.out.println("Error in cypher");
			success = false;
		}
		if(success)
			System.out.println("Cypher success! Check new file: " + salida );
	}
	
	public static void signature(String file, KeyPair keyPair, String sign_alg){
		boolean success = true;
		String salida = "ERROR";
		try{
			FileInputStream input = new FileInputStream(file);
			FileOutputStream output = new FileOutputStream(file
					.replaceAll("\\.\\w+", "") + ".firm");
			salida = file
					.replaceAll("\\.\\w+", "") + ".firm";
			Signature dsa = Signature.getInstance(sign_alg);
			if (keyPair != null) {
				dsa.initSign(keyPair.getPrivate());
				int i, j = 0;
				while ((i = input.read()) != -1) {
					dsa.update((byte) i);
					j++;
				}
				byte[] sig = dsa.sign();
				Header header = new Header(sign_alg, sig);
				header.save(output);
				input = new FileInputStream(file);
				while ((i = input.read()) != -1) {
					output.write(i);
				}
			}
			
	}catch (Exception e){
		System.out.println("Error in signature");
		success = false;
	}
		if(success)
			System.out.println("Signature success! Check new file: " + salida );
	}
	
	public static void check_signature(String file, KeyPair keyPair){
		boolean success = true;
		String salida = "ERROR";
		try {
			Header header = new Header();
			FileInputStream input = new FileInputStream(file);
			header.load(input);
			
			FileOutputStream output = new FileOutputStream(file
					.replaceAll("\\.\\w+", "") + ".ver");
			salida = file
					.replaceAll("\\.\\w+", "") + ".ver";
			if (keyPair != null) {
				Signature dsa = Signature
						.getInstance(header.getSigner());
				dsa.initVerify(keyPair.getPublic());
				int i, j = 0;
				while ((i = input.read()) != -1) {
					j++;
					dsa.update((byte) i);
					output.write(i);
				}
				
				if (dsa.verify(header.getSign())) {
					System.out.println("Correct signature!");

				} else {
					System.out.println("Incorrect signature!");
				}

				input.close();
				output.close();
			}
			
		 }catch(Exception e){
			 System.out.println("Error in verification");
			 success = false;
		 }
		if(success)
			System.out.println("Signature check success! Check new file: " + salida );
	}
	

}


